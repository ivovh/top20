# TODO
# 1. Should show artist, track, release year and ranking DONE
# 2. Could actual data from Top 2000 DONE
# 3. Should be able search for tracks (year DONE, title DONE, artist DONE)
# 4. Should be able to vote on tracks DONE
# 4.a. Save votes to file DONE
# 4.b. Show track in voting order DONE
# * Should use a database DONE
# * Should be a website DONE
# 5. Should show how many places it went up or down compared to last year
# 6. Should be able to enter data ourselves

from ast import pattern
import random
from datetime import datetime
import os, webbrowser

DATABASE = 'tracks.db'

MENU = """
1. Search for tracks
2. Show track list in ranking order
3. Show track list in voting order
4. Save tracks to CSV
5. List all artists ordered by name
6. Save tracks as website
0. Exit program
"""

SEARCH_MENU = """
Select property to search on:
1. Title
2. Artist
3. Release year
0. Go back to main menu
"""

def main():
    track_list = readDataFile()

    while True:
        input_number = input(MENU)
        if input_number == "1":
            search_menu(track_list)
        elif input_number == "2":
            show_tracklist_ranking(track_list)
        elif input_number == "3":
            show_tracklist_voting(track_list)
        elif input_number == "4":
            save_tracks_to_csv(track_list)
        elif input_number == "5":
            get_artists_from_database()
        elif input_number == "6":
            save_tracks_as_website(track_list)
        elif input_number == "0":
            return
        else:
            print("Invalid choice, try again.")

def search_menu(track_list):
    while True:
        choice = input(SEARCH_MENU)
        if choice == "1":
            search_on_keyword('title', track_list)
        elif choice == "2":
            search_on_keyword('artist', track_list)
        elif choice == "3":
            search_on_release_year(track_list)
        elif choice == "0":
            return
        else:
            print("Incorrect search option, try again.")

def save_tracks_as_website(tracks):
    output = "<html><head><link rel='stylesheet' href='style.css' /><title>Top 2000 website</title></head><body><h1>Top 2000 website!!@111</h1>"
    output += "<table><tr><th>#</th><th>Artist</th><th>Title</th></tr>"
    for track in tracks:
        artist_for_url = track['artist'].replace(' ', '_')
        artist = f"<a href=\"https://nl.wikipedia.org/wiki/{artist_for_url}\" target=\"_blank\">{track['artist']}</a>"

        title_and_artist_for_url = track['artist'].replace(' ', '+') + '+' + track['title'].replace(' ', '+')
        title = f"<a href=\"https://www.youtube.com/results?search_query={title_and_artist_for_url}\" target=\"_blank\">{track['title']}</a>"

        output += f"<tr><td>{track['ranking']}</td><td>{artist}</td><td>{title}</td></tr>"
    output += "</table></body></html>"
    with open('index.html', 'w') as file:
        file.write(output)
    browser = webbrowser.get('safari')
    browser.open('file:///Users/ihu03/src/coding-sessions/top20/index.html')

def get_artists_from_database():
    limit = input("How many artists do you want to see? ")
    pattern = input("Enter (part of) the artist's name: ")
    with os.popen(f'sqlite3 tracks.db "SELECT name FROM artists WHERE name LIKE \'%{pattern}%\' ORDER BY name LIMIT {limit}"') as db:
        print(db.read())

def search_on_release_year(tracks):
    results = []
    requested_year = input("Please enter the release year you want to search for: ")

    for track in tracks:
        if track['release_year'] == requested_year:
            results.append(track)
    
    if len(results) == 0:
        print("No tracks found for this release year :(")
    
    printTrackList(results)

def search_on_keyword(field_to_search, tracks):
    results = []
    keyword = input(f"Please enter a keyword to search for in {field_to_search}: ")

    for track in tracks:
        if keyword.lower() in track[field_to_search].lower():
            results.append(track)
    
    if len(results) == 0:
        print(f"No tracks found for this {field_to_search} keyword :(")
    
    printTrackList(results)

def show_tracklist_ranking(track_list):
    first = get_track_number_input("first", len(track_list))
    second = get_track_number_input("last", len(track_list))

    # Start list with the lowest number and end with the highest number
    if first > second:
        printTrackList(track_list, second, first)
    else:
        printTrackList(track_list, first, second)

def show_tracklist_voting(track_list):
    sorted_tracks = []

    # Count the votes for all tracks and sort them in order
    for track in track_list:
        if track['votes'] == 0:
            continue
        
        if len(sorted_tracks) == 0:
            # List is empty, so just append
            sorted_tracks.append(track)
        else:
            for i in range(len(sorted_tracks)):
                if track['votes'] > sorted_tracks[i]['votes']:
                    sorted_tracks.insert(i, track)
                    break
    
    # Display tracks in order
    printTrackList(sorted_tracks)

def get_track_number_input(type_of_number='', max_number=2000):
    while True:
        number = input(f"What is the {type_of_number} song number that you want to see? ")
        if not number.isdigit():
            print("Not a number! Try again")
            continue
        number = int(number)
        if number < 1 or number > max_number:
            print("Number out of range!")
            continue
        return number

def readDataFile():
    with open('top2000-2020.csv', 'r') as file:
        file.readline()

        tracks = []
        for line in file.readlines():
            line = line.strip()
            broadcast_year, ranking, artist, title, release_year = line.split(';')
            song = {
                'broadcast_year': broadcast_year,
                'ranking': ranking,
                'artist': artist,
                'title': title,
                'release_year': release_year,
                'votes': random.randint(0, 2000)
            }
            tracks.append(song)
        
        return tracks

def printTrackList(track_list, start=1, end=-1):
    # Select only the tracks according to the specified start and end ranking
    track_selection = track_list[start-1:end]
    for track in track_selection:
        track_info = f"""Title: {track['title']}
Artist: {track['artist']}
Release Year: {track['release_year']}
Ranking: {track['ranking']}
Votes: {track['votes']}
"""
        print(track_info)
        command = input("Press return to continue, v to vote, q to quit: ")
        if command == 'q':
            break
        elif command == 'v':
            track['votes'] += 1
            print(f"{track['title']} now has {track['votes']} votes!")
            print()

def save_tracks_to_csv(tracks):
    with open('saved_tracks.csv', 'w') as csv:
        csv.write("UitzendJr;Notering;Artiest;Titel;PublJr;Stemmen\n")
        current_year = datetime.now().year

        for track in tracks:
            csv.write(f"{current_year};{track['ranking']};{track['artist']};{track['title']};{track['release_year']};{track['votes']}\n")
    
    print(f"{len(tracks)} tracks were written to saved_tracks.csv")

if __name__=="__main__":
    main()